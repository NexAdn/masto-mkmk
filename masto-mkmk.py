#!/usr/bin/env python3

import bs4
import requests

from typing import TypedDict
from dataclasses import dataclass


BI_CLUB_HOMEPAGE = "https://www.bi-club.de"


@dataclass
class FoodItem:
    type: str
    food: str
    price: str

    def __str__(self) -> str:
        return f"{self.type}: {self.food} ({self.price})"


def main() -> int:
    page_text = fetch_page()
    soup = bs4.BeautifulSoup(page_text, "html.parser")

    food_list = soup.find("div", "happa")

    if not food_list:
        print("No food list found!")
        return 1

    food_table = food_list.find("table")
    assert food_table

    food = parse_food_table(food_table.tbody)

    if len(food) == 0:
        print("Food list is empty!")
        return 2

    print(format_food(food))
    return 0


def fetch_page() -> str:
    page = requests.get(BI_CLUB_HOMEPAGE)
    assert page.text
    return page.text


def parse_food_table(food_table: bs4.BeautifulSoup) -> list[FoodItem]:
    assert food_table

    food = []

    for row in food_table.find_all("tr"):
        food.append(
            FoodItem(
                type=row.find("td", "field-sp-typ").string,
                food=row.find("td", "field-sp-was").string,
                price=row.find("td", "field-sp-preis").string,
            )
        )

    return food


def format_food(food: list[FoodItem]) -> str:
    text = "Heute gibt es in der bi-Club Montagsküche:\n\n\n\n"

    text += "\n\n".join(map(str, food))

    return text


if __name__ == "__main__":
    exit(main())
